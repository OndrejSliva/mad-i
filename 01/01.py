import pandas as pd
import itertools

dataset = pd.read_csv('data.csv')
X = dataset[['outlook', 'Temperature_Nominal', 'Humidity_Nominal', 'Windy']]
Y = dataset[['Play']]

def are_combinations_same():
    return len(Y['Play'].unique()) == 1


def get_Y_val_at_index(index):
    return Y.loc[index]['Play']


def append_to_list_same(lst):
    dict = {}
    dict['col'] = 'else'
    dict['play'] = Y['Play'].unique()[0]
    lst.append(dict)
    return lst


column_combinations = {}
#gets column combinations
for i in range(1, len(X.columns)):
    c = itertools.combinations(X.columns, i)
    column_combinations[i] = []
    for x in c:
        column_combinations[i].append(x)

def get_unique_vals(column_name):
    return X[column_name].unique()

def get_unique_vals_in_dataset(column_name, dataset):
    return dataset[column_name].unique()


def get_sub_datasets_filtered_by(old_dataset, column_name):
    new_datasets = []
    for column_value in get_unique_vals(column_name):
        for dataset in old_dataset:
            new_datasets.append(dataset[dataset[column_name] == column_value])
    return new_datasets


def get_datasets(column_names):
    datasets = [X]
    for column_name in column_names:
        datasets = get_sub_datasets_filtered_by(datasets, column_name)
    return datasets


def get_list():
    lst = []
    for count_of_columns in column_combinations:
        if are_combinations_same():
            lst = append_to_list_same(lst)
            break

        for column_names in column_combinations[count_of_columns]:
            datasets = get_datasets(column_names)
            for dataset in datasets:
                Y_value = None
                is_same = True
                if len(dataset.index) == 0:
                    continue
                for index in dataset.index:

                    if Y_value is None:
                        Y_value = get_Y_val_at_index(index)
                    elif get_Y_val_at_index(index) != Y_value:
                        is_same = False
                        break

                if is_same:
                    arr = []
                    for column_index in dataset.columns:
                        if len(get_unique_vals_in_dataset(column_index, dataset)) == 1:
                            dict = {}
                            dict['col'] = column_index
                            dict['val'] = get_unique_vals_in_dataset(column_index, dataset)[0]
                            dict['play'] = Y_value
                            arr.append(dict)
                    if len(arr) != count_of_columns:
                        continue
                    lst.append(arr)
                    for i in dataset.index:
                        try:
                            global X
                            X = X.drop(index=[i])
                        except KeyError:
                            pass
                    if are_combinations_same():
                        lst = append_to_list_same(lst)
                        return lst
    return lst

lst = get_list()
s = ''
for rules in lst:
    str = ''
    for index, rule in enumerate(rules):
        if index == 0:
            str += 'if '
            yes_no = rule['play']
        else:
            str += ' and '
        if rule['val'] == True:
            rule['val'] = 'True'
        elif rule['val'] == False:
            rule['val'] = 'False'
        str += rule['col'] + ' = ' + rule['val']
    str += ' than ' + yes_no
    s += str + "\n"

print(s)