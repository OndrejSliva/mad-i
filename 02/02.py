from builtins import print

import pandas as pd
import itertools
import copy

dataset = pd.read_csv('data.csv')
X = dataset[['outlook', 'Temperature_Nominal', 'Humidity_Nominal', 'Windy']]
Y = dataset[['Play']]


class Filter:
    def __init__(self, column, value):
        self.column = column
        self.value = value

    def __str__(self):
        return f"{self.column}: {self.value}"


class SubDataset:
    def __init__(self, _dataset, _filter):
        self.dataset = _dataset
        self.filter = _filter

    def __str__(self):
        s = ""
        for f in self.filter:
            s += f"{f.__str__()} "

        return f"filter:{s} \n {self.dataset.__str__()}"


class ResultRow:
    def __init__(self, _filter, _count, _confidence, _result):
        self.filter = _filter
        self.count = _count
        self.support = round((self.count * 100) / len(X), 2)
        self.confidence = _confidence * 100
        self.result = _result

    def __str__(self):
        s = ""
        for f in self.filter:
            s += f" {f.__str__()}"

        return f"filter:{s}, result: {self.result}, support:{self.support}%, confidence: {self.confidence}%, count: {self.count}"


column_combinations = {}
#gets column combinations
for i in range(0, len(X.columns) + 1):
    c = itertools.combinations(X.columns, i)
    column_combinations[i] = []
    for x in c:
        column_combinations[i].append(x)


def get_unique_vals(column_name):
    return X[column_name].unique()


def get_sub_datasets_filtered_by(old_dataset, column_name):
    new_datasets = []
    for column_value in get_unique_vals(column_name):
        for dataset in old_dataset:
            x = copy.deepcopy(dataset)
            filt = Filter(column_name, column_value)
            ds = x.dataset[x.dataset[column_name] == column_value]
            f = x.filter
            f.append(filt)
            y = SubDataset(ds, f)
            new_datasets.append(y)
    return new_datasets


def get_datasets(column_names):
    datasets = [SubDataset(X, [])]

    for column_name in column_names:
        datasets = get_sub_datasets_filtered_by(datasets, column_name)
    return datasets


def get_confidence(result, indexes):
    res = 0
    for i in indexes:
        if Y.iloc[i]['Play'] == result:
            res += 1
    return res / len(indexes)


def get_list():
    lst = []
    for count_of_columns in column_combinations:
        for column_names in column_combinations[count_of_columns]:
            datasets = get_datasets(column_names)
            for dataset in datasets:
                for result in ['yes', 'no']:
                    if len(dataset.dataset) == 0:
                        confidence = 0
                    else:
                        confidence = get_confidence(result, dataset.dataset.index)
                    row = ResultRow(dataset.filter, len(dataset.dataset), confidence, result)
                    lst.append(row)
    return lst


lst = get_list()
for l in lst:
    print(l)
