from matplotlib import pyplot as plt

cetnost = {}
list = []

with open('KarateClub.csv', 'r') as f:
    for line in f.readlines():
        line = line.strip('\n')
        s = line.split(';')
        first_item = int(s[0])
        second_item = int(s[1])
        try:
            cetnost[first_item] += 1
        except KeyError:
            cetnost[first_item] = 1
        try:
            cetnost[second_item] += 1
        except KeyError:
            cetnost[second_item] = 1
        list.append([first_item, second_item])

neighbour_matrix = [[]] * len(cetnost)
for i in range(len(cetnost)):
    neighbour_matrix[i] = [0] * len(cetnost)

# vytvoří matici sousednosti
for line in list:
    first_item = int(line[0]) - 1
    second_item = int(line[1]) - 1
    neighbour_matrix[first_item][second_item] = 1
    neighbour_matrix[second_item][first_item] = 1

# jeden typ seznamu
for i in list:
    print(i)

def print_matrix(matrix):
    for row in matrix:
        print(row)

# matice sousednosti
print_matrix(neighbour_matrix)
# min stupeň
print(min(cetnost.values()))
# max stupeň
print(max(cetnost.values()))
# průměrný stupeň vrcholů
print(sum(cetnost.values()) / len(cetnost.keys()))

relative_cestnost = {}
arr = {}
for i in cetnost:
    try:
        arr[cetnost[i]] += 1
    except KeyError:
        arr[cetnost[i]] = 1
    relative_cestnost[i] = cetnost[i] / len(cetnost)


for i in range(1, len(cetnost) + 1):
    print(f"{i}: Četnost: {cetnost[i]} Relativní četnost: {relative_cestnost[i]}")

plt.bar(arr.keys(), arr.values())
plt.xticks(range(1, max(arr.keys()) + 1))
plt.show()