from matplotlib import pyplot as plt
import copy
import numpy as np

cetnost = {}
lst = []

with open('KarateClub.csv', 'r') as f:
    for line in f.readlines():
        line = line.strip('\n')
        s = line.split(';')
        first_item = int(s[0])
        second_item = int(s[1])
        try:
            cetnost[first_item] += 1
        except KeyError:
            cetnost[first_item] = 1
        try:
            cetnost[second_item] += 1
        except KeyError:
            cetnost[second_item] = 1
        lst.append([first_item, second_item])

neighbour_matrix = [[]] * len(cetnost)
for i in range(len(cetnost)):
    neighbour_matrix[i] = [0] * len(cetnost)

# vytvoří matici sousednosti
for line in lst:
    first_item = int(line[0]) - 1
    second_item = int(line[1]) - 1
    neighbour_matrix[first_item][second_item] = 1
    neighbour_matrix[second_item][first_item] = 1

# jeden typ seznamu
for i in lst:
    print(i)

def print_matrix(matrix):
    for row in matrix:
        print(row)

# matice sousednosti
# print_matrix(neighbour_matrix)
# min stupeň
# print(min(cetnost.values()))
# max stupeň
# print(max(cetnost.values()))
# průměrný stupeň vrcholů
# print(sum(cetnost.values()) / len(cetnost.keys()))

relative_cestnost = {}
arr = {}
for i in cetnost:
    try:
        arr[cetnost[i]] += 1
    except KeyError:
        arr[cetnost[i]] = 1
    relative_cestnost[i] = cetnost[i] / len(cetnost)


# for i in range(1, len(cetnost) + 1):
#     print(f"{i}: Četnost: {cetnost[i]} Relativní četnost: {relative_cestnost[i]}")

# plt.bar(arr.keys(), arr.values())
# plt.xticks(range(1, max(arr.keys()) + 1))
# plt.show()

# print_matrix(neighbour_matrix)

def floyd(matrix):
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if i == j:
                continue
            if matrix[i][j] == 0:
                matrix[i][j] = np.inf

    # dist = list(map(lambda i: list(map(lambda j: j, i)), matrix))     #todo wtf???
    dist = copy.deepcopy(matrix)
    for k in range(len(matrix)):
        for i in range(len(matrix)):
            for j in range(len(matrix)):
                dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j])

    return dist


distance_matrix = copy.deepcopy(neighbour_matrix)
distance_matrix = floyd(distance_matrix)

print_matrix(neighbour_matrix)
print('-----------------')
print_matrix(distance_matrix)


def get_average_distance(matrix):
    sum = 0
    count = 0
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if i != j:
                count += 1
                sum += matrix[i][j]
    return sum / count


print('average distnace')
print(get_average_distance(distance_matrix))


def get_average(matrix):
    return max([i for i in max(matrix)])


print('average')
print(get_average(distance_matrix))


def get_closeness_centrality(matrix):
    n = len(matrix)
    closeness = {}
    for i in range(1, n + 1):
        closeness[i] = n / sum(matrix[i - 1])
    return closeness


print('closeness centrality')
closeness_centrality = get_closeness_centrality(distance_matrix)
for idx in closeness_centrality:
    print(f"idx: {idx}: {closeness_centrality[idx]}")

