import pandas as pd
import numpy as np

df = pd.read_csv('iris.csv', sep=';')

X = df[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']]

total_mean_list = {}
total_mean_vector = []

for column in X.columns:
    mean = X[column].sum() / len(X)
    total_mean_list[column] = mean
    total_mean_vector.append(mean)

    variance = 0
    for row in X[column]:
        variance += (mean - row)**2
    variance = variance / len(X)
    print(f"Column: {column}: Mean: {mean}, Variance: {variance}")

sum = 0

for i, row in X.iterrows():
    row_variance = 0
    for column in X.columns:
        row_variance += (total_mean_list[column] - row[column]) ** 2

    sum += np.sqrt(row_variance)**2

total_variance = sum / len(X)

print(f"total mean: {total_mean_vector}, total variance: {total_variance}")
