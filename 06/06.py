# pro každý vypsat průměr a rozptyl + vypsat gausovu křivku
# spojité - roozděluju interval od do, u diskrétních počítám hodnoty
# bon prin - kldyž dostanu daovou sadu, hledám metodu, podle které se to snažím najít

# pro všechny sloupčky udělat distibuce - teoretické normální rozdělení (slide normal distribution s vzorcem), namalovat teoretickou distribuci a k ní doplnit empirickou

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import copy

df = pd.read_csv('iris.csv', sep=';')

X = df[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']]


def get_cumulative(values):
    result = copy.deepcopy(values)
    for idx, value in enumerate(values):
        if idx == 0:
            result[idx] = values[idx]
        else:
            result[idx] = values[idx] + result[idx - 1]
    return result/max(result)

for column in X.columns:
    mean = X[column].sum() / len(X)

    empirical_list = {}
    empirical_int_list = {}
    for value in X[column]:
        try:
            empirical_list[value] += 1
        except KeyError:
            empirical_list[value] = 1
        int_value = int(value) + 0.5
        try:
            empirical_int_list[int_value] += 1
        except KeyError:
            empirical_int_list[int_value] = 1

    for i in empirical_list:
        empirical_list[i] = empirical_list[i] / len(X)
    for i in empirical_int_list:
        empirical_int_list[i] = empirical_int_list[i] / len(X)

    variance = 0
    for row in X[column]:
        variance += (mean - row)**2
    variance = variance / len(X)

    x = np.arange(0, 10, .001)
    normal = (1 / np.sqrt(2 * np.pi * variance)) * np.exp(-(((x - mean) ** 2) / (2 * variance)))
    cumulative = get_cumulative(normal)
    plt.plot(x, normal, label='Normal distribution')
    plt.plot(x, cumulative, label='Cumulative distribution')
    # plt.scatter(empirical_list.keys(), empirical_list.values(), label='Empirical distribution')
    plt.scatter(empirical_int_list.keys(), empirical_int_list.values(), label='Empirical distribution')
    plt.legend()
    plt.show()