from matplotlib import pyplot as plt
import copy
import numpy as np

cetnost = {}
lst = []

with open('KarateClub.csv', 'r') as f:
    for line in f.readlines():
        line = line.strip('\n')
        s = line.split(';')
        first_item = int(s[0])
        second_item = int(s[1])
        try:
            cetnost[first_item] += 1
        except KeyError:
            cetnost[first_item] = 1
        try:
            cetnost[second_item] += 1
        except KeyError:
            cetnost[second_item] = 1
        lst.append([first_item, second_item])

neighbour_matrix = [[]] * len(cetnost)
for i in range(len(cetnost)):
    neighbour_matrix[i] = [0] * len(cetnost)

# vytvoří matici sousednosti
for line in lst:
    first_item = int(line[0]) - 1
    second_item = int(line[1]) - 1
    neighbour_matrix[first_item][second_item] = 1
    neighbour_matrix[second_item][first_item] = 1

# jeden typ seznamu
# for i in lst:
#     print(i)

def print_matrix(matrix):
    for row in matrix:
        print(row)

relative_cestnost = {}
arr = {}
for i in cetnost:
    try:
        arr[cetnost[i]] += 1
    except KeyError:
        arr[cetnost[i]] = 1
    relative_cestnost[i] = cetnost[i] / len(cetnost)

def floyd(matrix):
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if i == j:
                continue
            if matrix[i][j] == 0:
                matrix[i][j] = np.inf

    # dist = list(map(lambda i: list(map(lambda j: j, i)), matrix))     #todo wtf???
    dist = copy.deepcopy(matrix)
    for k in range(len(matrix)):
        for i in range(len(matrix)):
            for j in range(len(matrix)):
                dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j])

    return dist


def get_average_distance(matrix):
    sum = 0
    count = 0
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if i != j:
                count += 1
                sum += matrix[i][j]
    return sum / count


def get_average(matrix):
    return max([i for i in max(matrix)])


def get_closeness_centrality(matrix):
    n = len(matrix)
    closeness = {}
    for i in range(1, n + 1):
        closeness[i] = n / sum(matrix[i - 1])
    return closeness


# print_matrix(neighbour_matrix)

# for each row in nei_matrix
#     for each item in row
#         for each item in row
#             if item = act

clustering_coeficients = {}
for counting_index, counting_row in enumerate(neighbour_matrix):
    ni = sum(counting_row)
    mi = 0
    for neighbour_index, neighbour_value in enumerate(counting_row):  # prochází všechny sousedy
        if neighbour_index == counting_index:  # když je na aktuálně počítaném pokračuje
            continue
        if neighbour_value == 0:  # když není soused pokračuje
            continue

        neighbour_row = neighbour_matrix[neighbour_index]
        for idx, value in enumerate(neighbour_row):  # prochází sousedovy sousedy
            if idx == counting_index:  # pokud je aktuálně počítaný pokračuje
                continue
            if counting_row[idx] == 0:
                continue
            if value == 0:
                continue
            mi += 1

    mi /= 2

    if ni < 2:
        clustering_coeficients[str(counting_index + 1)] = 0
    else:
        clustering_coeficients[str(counting_index + 1)] = (2 * mi / (ni * (ni - 1)))

print("Shlukovací koeficient")

for i in clustering_coeficients:
    print(f"{i}: {clustering_coeficients[i]}")

print("Tranzitivita sítě")
print(sum(clustering_coeficients.values()) / len(clustering_coeficients.values()))

count_with_degree = {}
coefficient_with_degree = {}
clustering_effect = {}

for idx, row in enumerate(neighbour_matrix):
    degree = int(sum(row))

    try:
        count_with_degree[degree] += 1
        coefficient_with_degree[degree] += clustering_coeficients[str(idx + 1)]
        clustering_effect[degree] = coefficient_with_degree[degree] / count_with_degree[degree]
    except KeyError:
        count_with_degree[degree] = 1
        coefficient_with_degree[degree] = clustering_coeficients[str(idx + 1)]
        clustering_effect[degree] = coefficient_with_degree[degree] / count_with_degree[degree]

xs = [float(x) for x in clustering_effect.keys()]
ys = [float(y) for y in clustering_effect.values()]
plt.scatter(xs, ys)
plt.xlabel('d')
plt.ylabel('average')
plt.show()
