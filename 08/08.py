from matplotlib import pyplot as plt
import copy
import numpy as np
import random


class Solution:
    def __init__(self, vertexes, edges):
        self.__edges = edges
        self.__vertexes = vertexes
        self.__frequency = self.count_frequency()
        print('frequencies')
        self.__neighbour_matrix = self.count_neighbour_matrix()
        print('matrix')
        self.__distance_matrix = None
        self.__distance_matrix = self.get_distance_matrix()
        print('distance')
        self.__clustering_coefficients = self.get_clustering_coefficients()
        print('clus')
        self.__components = self.get_components()
        print('comp')

    def get_empty_list(self, default_value):
        lst = {}
        for i in range(len(self.__vertexes)):
            lst[i + 1] = default_value
        return lst

    def count_frequency(self):
        frequency = self.get_empty_list(0)
        for edge in self.__edges:
            first_item = int(edge[0])
            second_item = int(edge[1])
            frequency[first_item] += 1
            frequency[second_item] += 1
        return frequency

    def display_relative_frequency(self, name):
        arr = {}
        for i in self.__frequency:
            try:
                arr[self.__frequency[i]] += 1
            except KeyError:
                arr[self.__frequency[i]] = 1

        plt.bar(arr.keys(), arr.values())
        plt.xticks(range(0, max(arr.keys()) + 1))
        plt.savefig(name)
        plt.show()

    def count_neighbour_matrix(self):
        neighbour_matrix = [[0 for _ in range(len(self.__vertexes))] for _ in range(len(self.__vertexes))]
        for edge in self.__edges:
            first_item = int(edge[0]) - 1
            second_item = int(edge[1]) - 1
            neighbour_matrix[first_item][second_item] = 1
            neighbour_matrix[second_item][first_item] = 1
        return neighbour_matrix

    def get_min_degree(self):
        return min(self.__frequency.values())

    def get_max_degree(self):
        return max(self.__frequency.values())

    def get_average_degree(self):
        return sum(self.__frequency.values()) / len(self.__vertexes)

    def get_relative_frequency(self):
        relative_frequency = self.get_empty_list(0)
        for i in self.__frequency:
            relative_frequency[i] = self.__frequency[i] / len(self.__vertexes)
        return relative_frequency

    def get_distance_matrix(self):
        if self.__distance_matrix is not None:
            return self.__distance_matrix
        distance_matrix = copy.deepcopy(self.__neighbour_matrix)
        for i in range(len(self.__vertexes)):
            for j in range(len(self.__vertexes)):
                if i == j:
                    continue
                if distance_matrix[i][j] == 0:
                    distance_matrix[i][j] = np.inf

        for k in range(len(self.__vertexes)):
            print(k)
            for i in range(len(self.__vertexes)):
                for j in range(len(self.__vertexes)):
                    distance_matrix[i][j] = min(distance_matrix[i][j], distance_matrix[i][k] + distance_matrix[k][j])

        return distance_matrix

    def get_average_distance(self):
        count = 0
        sum = 0
        for i in range(len(self.__vertexes)):
            for j in range(len(self.__vertexes)):
                if i != j:
                    count += 1
                    sum += self.__distance_matrix[i][j]
        return sum / count

    def get_average(self):
        return max([i for i in max(self.__distance_matrix)])

    def get_closeness_centrality(self):
        n = len(self.__vertexes)
        closeness = {}
        for i in range(1, n + 1):
            closeness[i] = n / sum(self.__distance_matrix[i - 1])
        return closeness

    def get_clustering_coefficients(self):
        clustering_coeficients = {}
        for counting_index, counting_row in enumerate(self.__neighbour_matrix):
            ni = sum(counting_row)
            mi = 0
            for neighbour_index, neighbour_value in enumerate(counting_row):  # prochází všechny sousedy
                if neighbour_index == counting_index:  # když je na aktuálně počítaném pokračuje
                    continue
                if neighbour_value == 0:  # když není soused pokračuje
                    continue

                neighbour_row = self.__neighbour_matrix[neighbour_index]
                for idx, value in enumerate(neighbour_row):  # prochází sousedovy sousedy
                    if idx == counting_index:  # pokud je aktuálně počítaný pokračuje
                        continue
                    if counting_row[idx] == 0:
                        continue
                    if value == 0:
                        continue
                    mi += 1
            mi /= 2

            if ni < 2:
                clustering_coeficients[str(counting_index + 1)] = 0
            else:
                clustering_coeficients[str(counting_index + 1)] = (2 * mi / (ni * (ni - 1)))
        return clustering_coeficients

    def print_distribution_average_CC_against_degree(self, name):
        count_with_degree = {}
        coefficient_with_degree = {}
        clustering_effect = {}

        for idx, row in enumerate(self.__neighbour_matrix):
            degree = int(sum(row))

            try:
                count_with_degree[degree] += 1
                coefficient_with_degree[degree] += self.__clustering_coefficients[str(idx + 1)]
                clustering_effect[degree] = coefficient_with_degree[degree] / count_with_degree[degree]
            except KeyError:
                count_with_degree[degree] = 1
                coefficient_with_degree[degree] = self.__clustering_coefficients[str(idx + 1)]
                clustering_effect[degree] = coefficient_with_degree[degree] / count_with_degree[degree]

        xs = [float(x) for x in clustering_effect.keys()]
        ys = [float(y) for y in clustering_effect.values()]
        plt.scatter(xs, ys)
        plt.xlabel('d')
        plt.ylabel('average')
        plt.savefig(name)
        plt.show()

    def network_transitivity(self):
        return sum(self.__clustering_coefficients.values()) / len(self.__clustering_coefficients.values())

    def DFSUtil(self, temp, v, visited):
        visited[v] = True
        temp.append(v)
        for idx, value in enumerate(self.__neighbour_matrix[v]):
            if not visited[idx] and value == 1:
                temp = self.DFSUtil(temp, idx, visited)
        return temp

    def get_components(self):
        visited = {}
        connected_components = []
        for i in range(len(self.__vertexes)):
            visited[i] = False
        for v in range(len(self.__vertexes)):
            if not visited[v]:
                temp = []
                component = self.DFSUtil(temp, v, visited)
                connected_components.append(Component(component, self.__neighbour_matrix, self.__distance_matrix))

        return connected_components

    def get_count_of_components(self):
        return len(self.__components)

    def get_component_size_distribution(self):
        sizes = {}
        for component in self.__components:
            component_size = len(component)
            try:
                sizes[component_size] += 1
            except KeyError:
                sizes[component_size] = 1
        return dict(sorted(sizes.items()))

    def print_component_size_distribution(self, name):
        arr = {}
        for i, component in enumerate(self.__components):
            try:
                arr[len(self.__components[i])] += 1
            except KeyError:
                arr[len(self.__components[i])] = 1

        plt.bar(arr.keys(), arr.values())
        plt.xticks(range(1, max(arr.keys()) + 1))
        plt.savefig(name)
        plt.show()

        # sizes = {}
        # for component in self.__components:
        #     component_size = len(component)
        #     try:
        #         sizes[component_size] += 1
        #     except KeyError:
        #         sizes[component_size] = 1
        # return dict(sorted(sizes.items()))


class Component:
    def __init__(self, vertexes, neighbour_matrix, distance_matrix):
        self.__vertexes = vertexes
        self.__neighbour_matrix = neighbour_matrix
        self.__distance_matrix = distance_matrix
        self.__average_distance = self.get_average_distance()
        self.__average = self.get_average()

    def get_average_distance(self):
        if len(self.__vertexes) == 1:
            return 0

        count = 0
        sum = 0
        for vertex_i in self.__vertexes:
            for vertex_j in self.__vertexes:
                if vertex_i != vertex_j:
                    count += 1
                    sum += self.__distance_matrix[vertex_i][vertex_j]

        return sum / count

    def get_average(self):
        max = 0
        for i in self.__vertexes:
            for j in self.__vertexes:
                if self.__distance_matrix[i][j] > max:
                    max = self.__distance_matrix[i][j]
        return max

    def __len__(self):
        return len(self.__vertexes)

    def __str__(self):
        return f"{self.__vertexes}, Average_distance: {self.__average_distance}, Průměr: {self.__average}"


def print_matrix(matrix):
    for row in matrix:
        print(row)


def print_list(lst):
    for item in lst:
        print(f"{item}: {lst[item]}")


def print_components(components):
    s = ""
    for component in components:
        s += str(component)
        print(component)
    return s


def generate_graph(num_of_vertexes, propability):
    edge_list = []
    vertexis = {}

    max_edges = (num_of_vertexes * (num_of_vertexes - 1)) / 2

    for i in range(num_of_vertexes):
        vertexis[i + 1] = True

    for i in range(num_of_vertexes):
        for j in range(i + 1, num_of_vertexes):
            rand_prop = random.uniform(0, 1)
            if rand_prop < propability:
                edge_list.append([i+1, j+1])

    return vertexis, edge_list


def run(vertexies, edge_list, name):
    res = ""
    s = Solution(vertexies, edge_list)
    print('------------------------Matice vzdáleností------------------------')
    # print_matrix(s.count_neighbour_matrix())
    print('------------------------Minimální stupeň------------------------')
    print(s.get_min_degree())
    print('------------------------Maxcximální stupeň------------------------')
    print(s.get_max_degree())
    print('------------------------Průměrný stupeň------------------------')
    print(s.get_average_degree())
    print('------------------------Relativní četnost------------------------')
    # print_list(s.get_relative_frequency())
    s.display_relative_frequency(f"rel_freq {name}")
    print('------------------------Matice vzdáleností------------------------')
    # print_matrix(s.get_distance_matrix())
    print('------------------------Průměrná vzálenost------------------------')
    print(s.get_average_distance())
    print('------------------------Průměr------------------------')
    print(s.get_average())
    print('------------------------Closeness centrality------------------------')
    # print_list(s.get_closeness_centrality())
    print('------------------------Shlukovací koeficient------------------------')
    # print_list(s.get_clustering_coefficients())
    print(s.network_transitivity())
    print('------------------------Distribuce průměrného cc vůči stupni------------------------')
    s.print_distribution_average_CC_against_degree(f"ditribution_average_cc_against_degree {name}")
    print("------------------------komponenty------------------------")
    print_components(s.get_components())
    print("------------------------Počet komponent------------------------")
    print(s.get_count_of_components())
    print("------------------------Distribuce velikosti komponent souvislosti)------------------------")
    s.print_component_size_distribution(f"component size distribution {name}")


#1
vertexies, edge_list = generate_graph(500, 0.00200401)
# vertexies, edge_list = generate_graph(200, 0.005025125)
run(vertexies, edge_list, '=1')
#<1
vertexies, edge_list = generate_graph(500, 0.0015)
# vertexies, edge_list = generate_graph(200, 0.004)
run(vertexies, edge_list, 'lt1')
#>1
vertexies, edge_list = generate_graph(500, 0.0025)
# vertexies, edge_list = generate_graph(200, 0.008)
run(vertexies, edge_list, 'gt1')