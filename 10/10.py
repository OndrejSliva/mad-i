#birch - dataset

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')
import numpy as np


class KMeans:

    def __init__(self, k=2, tol=0.00001, max_iter=300):
        self.k = k
        self.tol = tol
        self.max_iter = max_iter
        self.centroids = {}
        self.classifications = {}
        self.means = {}
        self.stdevs = {}

    def fit(self, dataset: pd.DataFrame, normalize=False):
        if normalize:
            dataset = self.normalize(dataset)

        dataset = dataset.to_numpy()
        self.centroids = self.__get_random_centroids(dataset)

        for i in range(self.max_iter):
            print(i)
            self.classifications = {}

            for i in range(self.k):
                self.classifications[i] = []

            for featureset in dataset:
                distances = [np.linalg.norm(featureset - self.centroids[centroid]) for centroid in self.centroids]
                classification = distances.index(min(distances))
                self.classifications[classification].append(featureset)

            prev_centroids = dict(self.centroids)

            for classification in self.classifications:
                self.centroids[classification] = np.average(self.classifications[classification], axis=0)

            optimalized = True

            for c in self.centroids:
                original_centroid = prev_centroids[c]
                current_centroid = self.centroids[c]
                # print(np.sum((current_centroid - original_centroid) / original_centroid * 100.0))
                if np.sum((current_centroid - original_centroid) / original_centroid * 100.0) > self.tol:
                    optimalized = False

            if optimalized:
                break

    def normalize(self, data: pd.DataFrame):
        self.means = {}
        self.stdevs = {}
        for column in data.columns:
            data[column] = pd.to_numeric(data[column])
            column_sum = data[column].sum()
            mean = column_sum / len(data)
            stdev = 0
            for i in range(len(data)):
                stdev += (data[column][i] - mean) ** 2
            stdev = np.sqrt(stdev / len(data))

            for i in range(len(data)):
                data[column] = data[column].astype(float)
                data.at[i, column] = float(data[column][i]) - float(mean)
                data.at[i, column] = float(data[column][i]) / float(stdev)

            self.means[column] = mean
            self.stdevs[column] = stdev

        return data

    def __get_random_centroids(self, data):
        count_fo_points = len(data)
        needs_to_be_generated = self.k
        indexes = []
        centroids = {}

        while needs_to_be_generated > 0:
            idx = np.random.randint(0, count_fo_points)
            if idx in indexes:
                continue
            centroids[len(indexes)] = data[idx]
            indexes.append(idx)
            needs_to_be_generated -= 1
        return centroids


def run(path_to_dataset, attributes, k, normalize_data = False):
    df = pd.read_csv(path_to_dataset, sep=';')
    X = df[attributes]


    is_2D = len(attributes) == 2

    clf = KMeans(k)
    clf.fit(X, normalize_data)

    colors = ['g', 'r', 'c', 'b', 'k', 'o']

    for i, centroid in enumerate(clf.centroids):
        print(clf.centroids[i])
        if is_2D:
            plt.scatter(clf.centroids[centroid][0], clf.centroids[centroid][1], marker='o', color='k', s=150,
                        linewidths=5)

    if is_2D:
        for classification in clf.classifications:
            color = colors[classification]
            for featureset in clf.classifications[classification]:
                plt.scatter(featureset[0], featureset[1], marker='x', color=color, s=150, linewidths=5)

    plt.show()


run('iris.csv', ['sepal_length', 'sepal_width', 'petal_length', 'petal_width'], 2)
# run('iris.csv', ['petal_length', 'petal_width'], 2)
# run('iris.csv', ['sepal_length', 'petal_length'], 2)
# run('iris.csv', ['sepal_width', 'petal_width'], 2)
# run('iris.csv', ['sepal_length', 'sepal_width'], 2)
# run('dataset.csv', ['duration_of_eruption', 'waiting_time'], 2, normalize_data=True)
