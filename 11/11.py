#implementovat knn, nebo naive bias, knn musí jít volit počet a (ideálně zadávat liché čísla)
# jakou složitost mají

import numpy as np
import pandas as pd
import operator
from sklearn.utils import shuffle

import time

def k_nearest_neighbours(data, predict, data_columns, class_column, k):
    distances = []
    predict_valuest_list = predict[data_columns].to_list()
    # s = time.time()
    # print(data.head())
    d = data[data_columns]
    r = data[class_column]
    # print(d.head())
    # print(r.head())
    for idx, row in data.iterrows():
        features = row[data_columns].tolist()
        group = row[class_column]
        # print(row)
        # print(group)
        # print(r.iloc[idx] == group)
        # print(idx)
        # print(r.iloc[idx])
        # exit()
        euclidean_distance = 0
        for i in range(len(data_columns)):
            euclidean_distance += (features[i] - predict_valuest_list[i]) ** 2

        euclidean_distance = np.sqrt(euclidean_distance)
        # euclidean_distance = np.linalg.norm(np.array(features) - np.array(predict_valuest_list))

        distances.append([euclidean_distance, group])
    # print(time.time() - s)

    min_distances = [i[1] for i in sorted(distances)[:k]]
    res_dict = {}
    for i in min_distances:
        if i in res_dict:
            res_dict[i] += 1
        else:
            res_dict[i] = 1
    return max(res_dict.items(), key=operator.itemgetter(1))[0]


def run(dataset, num_of_parts, data_columns, class_column, k):
    dataset = shuffle(dataset)
    part_size = int(len(df) / num_of_parts)
    total_good = 0
    total_bad = 0
    for i in range(num_of_parts):
        first_idx = i * part_size
        second_idx = (i+1) * part_size
        first_part = dataset.iloc[:first_idx]
        second_part = dataset.iloc[second_idx:]
        test_part = dataset.iloc[first_idx:second_idx]
        train_part = pd.concat([first_part, second_part])
        good = 0
        bad = 0
        for index, row in test_part.iterrows():
            res = k_nearest_neighbours(train_part, row, data_columns, class_column, k)
            if row[class_column] == res:
                good += 1
            else:
                bad += 1
        print(f"Good: {good}, Bad: {bad}")
        total_good += good
        total_bad += bad
    print(f"Total good: {total_good}, Bad: {total_bad}")



print('Iris')
df = pd.read_csv("iris.csv", sep=';')
num_of_parts = 10
k = 5
run(df, num_of_parts, ['sepal_length', 'sepal_width', 'petal_length', 'petal_width'], 'variety', k)

print('Fruits')
df = pd.read_csv("fruits.csv", sep=';')
num_of_parts = 10
k = 5
run(df, num_of_parts, ['mass', 'width', 'height'], 'fruit_type', k)
run(df, num_of_parts, ['mass', 'width', 'height'], 'fruit_name', k)


