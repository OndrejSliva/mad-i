from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import tkinter as tk
from os import path
from tkinter import messagebox
import numpy as np
import matplotlib.pyplot as plt
import copy
import csv
import operator
import random

interface = Tk()


class Run:
    def __init__(self, file_name, k, fold):
        self.file_name = file_name
        self.dist_data = []
        self.array_data = []
        self.columns = []
        self.label = None
        self.attributes = []
        self.reset_data()
        self.mean = {}
        self.variance = {}
        self.k = k
        self.fold = fold

    def run(self):
        print("hello")
        self.reset_data()
        self.load_file()
        self.select_label()   #todo
        # self.label = 4          #todo
        # todo check missing label
        # print(self.label)
        # print(self.columns[self.label])
        self.select_attributes()      #todo
        # self.attributes = [0, 1, 2, 3]        #todo
        print("label")
        # print(self.columns[self.label])
        # print("attributes")
        # for i in self.attributes:
        #     print(self.columns[i])
        self.get_mean()
        self.add_missing_values()
        self.get_variance()
        print(self.mean)
        print(self.variance)
        # self.distributions()
        self.start_knn()

    def reset_data(self):
        self.array_data = []
        self.dist_data = []
        self.label = None
        self.attributes = []
        self.mean = {}
        self.variance = {}

    def load_file(self):
        with open(self.file_name, newline='') as csv_file:
            reader = csv.DictReader(csv_file, delimiter=',')
            self.columns = reader.fieldnames
            for row in reader:
                self.dist_data.append(row)
                self.array_data.append(list(row.values()))
        print(self.array_data[0][0])

    def select_label(self):
        def select(self):
            if self is None:        ##todo?
                print("tady")
                return
            self.label = v0.get()
            select_label_windows.destroy()

        select_label_windows = Toplevel(interface)
        select_label_windows.title("Select label column")
        height = len(self.columns) * 20 + 70
        select_label_windows.geometry(f"200x{height}")
        Label(select_label_windows, text="Select label column").pack()
        v0 = IntVar()
        v0.set(1)
        for i, val in enumerate(self.columns):
            r1 = Radiobutton(select_label_windows, text=val, variable=v0, value=i)
            r1.place(x=50, y=(20 + i * 20))

        select_label_button = ttk.Button(select_label_windows, text="Choose", command=lambda:select(self))  # <------
        select_label_button.place(x=50, y=(height-20))
        interface.wait_window(select_label_windows)

    def select_attributes(self):
        def select(self):
            for i in variables:
                if i.get() == 1:
                    columnt_idx = int(i._name)
                    self.attributes.append(columnt_idx)
            select_attributes_windows.destroy()

        select_attributes_windows = Toplevel(interface)
        select_attributes_windows.title("Select attributes columns")
        height = len(self.columns) * 20 + 50
        select_attributes_windows.geometry(f"200x{height}")
        Label(select_attributes_windows, text="Select label column").pack()
        variables = []
        for i, val in enumerate(self.columns):
            if i == self.label:
                continue
            var = IntVar(name=str(i))
            var.set(1)
            check = Checkbutton(select_attributes_windows, text=val, variable=var)
            check.place(x=50, y=(20 + len(variables) * 20))
            variables.append(var)

        select_attributes_button = ttk.Button(select_attributes_windows, text="Choose", command=lambda: select(self))  # <------
        select_attributes_button.place(x=50, y=(height - 20))
        interface.wait_window(select_attributes_windows)

    def get_mean(self):
        for column_idx in self.attributes:
            column_sum = 0
            count = 0
            for row_idx in range(len(self.dist_data)):
                str_val = self.array_data[row_idx][column_idx]
                if str_val != "":
                    value = float(str_val)
                    column_sum += value
                    count += 1
            mean = column_sum / count
            self.mean[column_idx] = mean

    def add_missing_values(self):
        for column_idx in self.attributes:
            mean = self.mean[column_idx]
            for row_idx in range(len(self.dist_data)):
                str_val = self.array_data[row_idx][column_idx]
                if str_val == "":
                    self.array_data[row_idx][column_idx] = mean

    def get_variance(self):
        for column_idx in self.attributes:
            variance = 0
            count = 0
            mean = self.mean[column_idx]
            for row_idx in range(len(self.dist_data)):
                str_val = self.array_data[row_idx][column_idx]
                if str_val != "":
                    value = float(str_val)
                    variance += (mean - value) ** 2
                    count += 1
            variance /= count
            self.variance[column_idx] = variance

    def distributions(self):
        names = []
        for i in self.attributes:
            names.append(self.columns[i])

        def get_cumulative(values):
            print(values)
            result = copy.deepcopy(values)
            for idx, value in enumerate(values):
                if idx == 0:
                    result[idx] = values[idx]
                else:
                    result[idx] = values[idx] + result[idx - 1]
            return result / max(result)

        for column_idx in self.attributes:
            mean = self.mean[column_idx]

            empirical_list = {}
            empirical_int_list = {}
            for row_idx in range(len(self.array_data)):
                value = float(self.array_data[row_idx][column_idx])
                try:
                    empirical_list[value] += 1
                except KeyError:
                    empirical_list[value] = 1
                int_value = int(value) + 0.5
                try:
                    empirical_int_list[int_value] += 1
                except KeyError:
                    empirical_int_list[int_value] = 1

            for i in empirical_list:
                empirical_list[i] = empirical_list[i] / len(self.array_data)
            for i in empirical_int_list:
                empirical_int_list[i] = empirical_int_list[i] / len(self.array_data)

            variance = self.variance[column_idx]

            x = np.arange(0, 10, .001)
            normal = (1 / np.sqrt(2 * np.pi * variance)) * np.exp(-(((x - mean) ** 2) / (2 * variance)))
            cumulative = get_cumulative(normal)
            plt.plot(x, normal, label='Normal distribution')
            plt.plot(x, cumulative, label='Cumulative distribution')
            plt.scatter(empirical_int_list.keys(), empirical_int_list.values(), label='Empirical distribution')
            plt.legend()
            plt.show()

    def start_knn(self):
        random.shuffle(self.array_data)
        part_size = int(len(self.array_data) / self.fold)
        total_good = 0
        total_bad = 0
        for i in range(self.fold):
            first_idx = i * part_size
            second_idx = (i + 1) * part_size
            first_part = self.array_data[:first_idx]
            second_part = self.array_data[second_idx:]
            test_part = self.array_data[first_idx:second_idx]
            train_part = first_part + second_part
            good = 0
            bad = 0

            for index, row in enumerate(test_part):
                res = self.k_nearest_neighbours(train_part, row)
                if row[self.label] == res:
                    good += 1
                else:
                    bad += 1
            print(f"Good: {good}, Bad: {bad}")
            total_good += good
            total_bad += bad
        print(f"Total good: {total_good}, Bad: {total_bad}")

    def k_nearest_neighbours(self, data, predict):
        distances = []
        for idx, row in enumerate(data):
            group = row[self.label]

            euclidean_distance = 0
            for i in self.attributes:
                euclidean_distance += (float(row[i]) - float(predict[i])) ** 2

            euclidean_distance = np.sqrt(euclidean_distance)

            distances.append([euclidean_distance, group])

        min_distances = [i[1] for i in sorted(distances)[:self.k]]
        res_dict = {}
        for i in min_distances:
            if i in res_dict:
                res_dict[i] += 1
            else:
                res_dict[i] = 1
        return max(res_dict.items(), key=operator.itemgetter(1))[0]


def openfile():
    print("tady")
    file_name = filedialog.askopenfilename()
    file_path_edit.delete(0, END)
    file_path_edit.insert(0, file_name)


def run():
    file_name = file_path_edit.get()
    if not path.exists(file_name):
        messagebox.showerror("Title", "Musíte zadat cestu k datasetu")
        return

    k = k_edit.get()
    if not k.isdigit():
        messagebox.showerror("K must be int")

    fold = fold_edit.get()
    if not fold.isdigit():
        messagebox.showerror("fold must be int")


    res = Run(file_name, int(k), int(fold))
    res.run()

file_path_label = tk.Label(interface, text="Path")
file_path_label.grid(column=0, row=1)

file_path_edit = tk.Entry(interface, width=50)
file_path_edit.grid(column=1, row=1)
file_path_edit.insert(0, "D:/PythonProjects/VSB/MAD/iris_updated.csv")
# file_path_edit.insert(0, "D:/PythonProjects/VSB/MAD/chess.csv")

k_label = tk.Label(interface, text="K")
k_label.grid(column=0, row=2)

k_edit = tk.Entry(interface, width=50)
k_edit.grid(column=1, row=2)
k_edit.insert(0, "3")

fold_label = tk.Label(interface, text="Fold")
fold_label.grid(column=0, row=3)

fold_edit = tk.Entry(interface, width=50)
fold_edit.grid(column=1, row=3)
fold_edit.insert(0, "10")

load_button = ttk.Button(interface, text="Open", command=openfile)  # <------
load_button.grid(column=2, row=1)

run_button = ttk.Button(interface, text="Run", command=run)  # <------
run_button.grid(column=2, row=4)

interface.geometry("400x300+10+10")
interface.mainloop()